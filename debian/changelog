bcal (2.4-1) unstable; urgency=medium

  * Import new upstream version
  * d/copyright:
    - Update the copyright year

 -- SZ Lin (林上智) <szlin@debian.org>  Sat, 05 Mar 2022 16:42:55 +0800

bcal (2.3-1) unstable; urgency=medium

  * Import new upstream version
  * d/control:
    - Bump debhelper-compat to 13
    - Bump Standards-Version to 4.6.0.1

 -- SZ Lin (林上智) <szlin@debian.org>  Sun, 17 Oct 2021 14:06:26 +0800

bcal (2.2-1) unstable; urgency=medium

  * Import new upstream version
  * d/control:
    - Bump Standards-Version to 4.5.0

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 27 Jan 2020 18:00:00 +0800

bcal (2.1+git20190806.6c8d325-3) unstable; urgency=medium

  * d/tests:
    - Fix regression

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 09 Dec 2019 17:28:26 +0800

bcal (2.1+git20190806.6c8d325-2) unstable; urgency=medium

  * d/control:
    - Add Suggests: bc to enhance the functionality of bcal
    - Add 64-bit architecture support (Closes: #942178)
    - Add Rules-Requires-Root: no
    - Bump Standards-Version to 4.4.1
    - Use debhelper-compat (= 12) in build-dependency to replace d/compat
  * d/tests:
    - Add test case

 -- SZ Lin (林上智) <szlin@debian.org>  Sat, 30 Nov 2019 23:22:36 +0800

bcal (2.1+git20190806.6c8d325-1) unstable; urgency=high

  * Import new upstream version
  * Fix FTBFS with GCC-9 (Closes: #925637)
  * d/compat:
    - Bump compat version to 12
  * d/control:
    - Bump Standards-Version to 4.4.0
    - Bump dephelper to 12
  * d/watch:
    - Fix wrong format in package naming

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 27 Aug 2019 10:14:38 +0800

bcal (2.1-1) unstable; urgency=medium

  * Import new upstream version
  * Add upstream metadata

 -- SZ Lin (林上智) <szlin@debian.org>  Thu, 13 Dec 2018 10:02:27 +0800

bcal (2.0-1) unstable; urgency=medium

  * Import new upstream version
  * d/control:
    * Bump Standards-Version to 4.2.1

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 03 Oct 2018 13:33:08 +0800

bcal (1.9-2) unstable; urgency=medium

  * Correct the package in depends: readline-common (Closes: #899361)

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 23 May 2018 20:39:55 +0800

bcal (1.9-1) unstable; urgency=medium

  * Import new upstream version
  * d/control: Bump Standards-Version to 4.1.4
  *            Add {build-}depends: readline-{devel}

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 22 May 2018 23:20:39 +0800

bcal (1.8-1) unstable; urgency=medium

  * Import new upstream version
  * d/control: Bump Standards-Version to 4.1.3
  *            Bump debhelper to 11
  * d/manpages: Use upstream manpage
  * d/compat: Bump compat version to 11
  * d/rules: Add pie feature

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 16 Mar 2018 13:11:20 +0800

bcal (1.7-1) unstable; urgency=medium

  * Import new upstream version
  * Bump Standards-Version to 4.1.1
  * Remove unnecessary option in d/rules
  * Replace "http" with "https" in d/copyright

 -- SZ Lin (林上智) <szlin@debian.org>  Sun, 08 Oct 2017 18:57:03 +0800

bcal (1.6-1) unstable; urgency=medium

  * Import new upstream version

 -- SZ Lin (林上智) <szlin@debian.org>  Sun, 02 Jul 2017 21:05:13 +0800

bcal (1.5-1) unstable; urgency=medium

  * New upstream version

 -- SZ Lin (林上智) <szlin@debian.org>  Sun, 16 Apr 2017 12:46:21 +0800

bcal (1.4-1) unstable; urgency=medium

  * Initial release. (Closes: #842225)

 -- SZ Lin (林上智) <szlin@cs.nctu.edu.tw>  Wed, 26 Oct 2016 15:27:09 +0800
